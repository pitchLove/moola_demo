//Javascript will go here
(function(jQuery, Firebase) {
    //url --> "https://s3-us-west-2.amazonaws.com/pitchlove-audio/" + vid + ".mp3"
    function inputSong(num) {
        var vid;
        if (num == 1) {
            vid  = "1457024370670";
        } else if (num == 2) {
            vid  = "1457916641868";
        } else {
            vid  = "1457194608563";
        }
        //put player in here
    }
    
    $(window).resize(function() {
        var path = $(this);
        var contW = path.width();
        if(contW >= 751){
            document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
        }else{
            document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
        }
    });
    $(document).ready(function() {
        $('.dropdown').on('show.bs.dropdown', function(e){
            $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
        });
        $('.dropdown').on('hide.bs.dropdown', function(e){
            $(this).find('.dropdown-menu').first().stop(true, true).slideUp(300);
        });
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            var elem = document.getElementById("sidebar-wrapper");
            left = window.getComputedStyle(elem,null).getPropertyValue("left");
            if(left == "200px"){
                document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
            }
            else if(left == "-200px"){
                document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
            }
        });
    });

    $('.sidebar-toggle ul.sidebar-nav').on('click', 'li .song', function(){
        var songSelected = $(this).attr('id');
        var songInfo = $(this).val();
        $('.panel').show();
        $('.panel-title').val(songInfo);
        var songNum = songSelected.split("g")[1];
        console.log(songNum);
        inputSong(songNum)
    });
    
    
    
}(window.jQuery, window.Firebase))
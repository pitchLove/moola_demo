/**
 * SongAPIController
 *
 * @description :: Server-side logic for managing songs
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    getSongs: function(req, res) {
        songAPIService.getSongs(function(songs) {
            res.json(songs);
        });
    },
    addSong: function(req, res) {
        var songVal = (req.body.value) ? req.body.value : undefined
        songAPIService.addSong(songVal, function(success) {
            res.json(success);
        });
    },
    playSong: function(req, res) {
       var songVal = (req.body.value) ? req.body.value : undefined
        songAPIService.playSong(songVal, function(success) {
            res.json(success);
        });
    }
};


/**
 * songAPI.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      value: {
      'type': 'text'
    }
  }
};

/*
Users
    Id
        Name
        Email
        Payment method
        Artist account (optional)
Songs
    Id
        Artist
        Name
        Album
        Length
        Cost
        Listens
        Total time
UserListens
    UserId
        Today
            SongId
            Listens
            Time
            $$$
        Week
            SongId
            Listens
            Time
            $$$
        All
            SongId
            Listens
            Time
            $$$
ArtistPlays
    UserId
        Today
            SongId
            Listens
            Time
            $$$
        Week
            SongId
            Listens
            Time
            $$$
        All
            SongId
            Listens
            Time
            $$$ 
*/
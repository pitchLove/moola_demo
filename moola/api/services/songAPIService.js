module.exports = {
  getSongs: function(next) {
    songAPI.find().exec(function(err, songs) {
      if(err) throw err;
      next(songs);
    });
  },
  addSong: function(songVal, next) {
    songAPI.create({value: songVal}).exec(function(err, song) {
      if(err) throw err;
      next(song);
    });
  },
  playSong: function(songVal, next) {
      /* Get song info from [songs] list
    Decrypt song 
    Start feeding in buffer
    Add to counter for user listens
    Add to counter for artist listens
    Launch {payments calculator}
    Send time listened and name when done
    Calculate time user listens once it is done
    Add payment to artist weekly buffer
    Add payment to user weekly buffer
    Add payment to MOOLA weekly buffer
    */
  }
};
'use strict';

var moolaMVP = angular.module('moolaMVP', ['ngRoute', 'ui.bootstrap']);
moolaMVP.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: '/templates/moola.html',
      controller: 'MoolaCtrl'
    }).otherwise({
      redirectTo: '/',
      caseInsensitiveMatch: true
    })
  }]);

moolaMVP.controller('MoolaCtrl', ['$scope', '$rootScope', 'SongService', function($scope, $rootScope, SongService) {
  $scope.formData = {};
  $scope.songs = [];

  SongService.getSongs().then(function(response) {
    $scope.songs = response;
  });

  $scope.addSong = function() {
    SongService.addSong($scope.formData).then(function(response) {
        console.log(response);
      $scope.songs.push($scope.formData)
      $scope.formData = {};
    });
  }
  
  $scope.playSong = function() {
    SongService.playSong($scope.formData).then(function(response) {
     /* Get song info from [songs] list
    Decrypt song 
    Start feeding in buffer
    Add to counter for user listens
    Add to counter for artist listens
    Launch {payments calculator}
    Send time listened and name when done
    Calculate time user listens once it is done
    Add payment to artist weekly buffer
    Add payment to user weekly buffer
    Add payment to MOOLA weekly buffer
    */
      $scope.formData = {};
    });
  }


}]);
moolaMVP.service('SongService', function($http, $q) {
  return {
    'getSongs': function() {
      var defer = $q.defer();
      $http.get('/songAPI/getSongs').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'addSong': function(song) {
        console.log(song);
      var defer = $q.defer();
      $http.post('/songAPI/addSong', song).success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    },
    'playSong': function(song) {
        console.log(song);
      var defer = $q.defer();
      $http.get('/songAPI/playSong', song).success(function(resp){
        defer.resolve(resp);
            /* Get song info from [songs] list
            Decrypt song 
            Start feeding in buffer
            Add to counter for user listens
            Add to counter for artist listens
            Launch {payments calculator}
            Send time listened and name when done
            Calculate time user listens once it is done
            Add payment to artist weekly buffer
            Add payment to user weekly buffer
            Add payment to MOOLA weekly buffer
            */
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    }
}});